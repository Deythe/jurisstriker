using UnityEngine;

public class Shoot : MonoBehaviour
{
    [SerializeField] private Rigidbody ballRigidbody;
    public float force;
    
    public void PlayerShoot(float horizontal, float vertical)
    {
        BallMovements.instance.isTaken = false;
        PlayerStatement.instance.p1AsBall = false;
        PlayerStatement.instance.p2AsBall = false;
        if ((horizontal + vertical )== 0)
        {
            ballRigidbody.AddForce(transform.forward*force); 
        }
        else
        {
            ballRigidbody.AddForce(transform.forward + new Vector3(horizontal,0,vertical)*force); 
        }
    }
}
