using UnityEngine;

public class InputManager : MonoBehaviour
{
    [SerializeField] private int player;
    [SerializeField] private string shoot = "2";

    [SerializeField] private PlayerMovement player1;
    [SerializeField] private Dash playerDash;
    [SerializeField] private Shoot playerShoot;

    void Update()
    {
        
        if (Input.GetAxis("Horizontal"+player)!=0 || Input.GetAxis("Vertical"+player)!=0)
        {
            player1.Movement(Input.GetAxis("Horizontal"+player),-Input.GetAxis("Vertical"+player));
        }
        
        if (Input.GetKeyDown("joystick " + player + " button 1"))
        {
            playerDash.PlayerDash();
        }
        
        if (Input.GetKeyDown("joystick " + player + " button 2"))
        {
            if(PlayerStatement.instance.PlayerHasTheBall(player)){
                playerShoot.PlayerShoot(Input.GetAxis("Horizontal" + player), -Input.GetAxis("Vertical" + player));
            }
        }

    }
}