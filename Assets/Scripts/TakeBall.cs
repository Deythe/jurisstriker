using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class TakeBall : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Ball"))
        {
            if (transform.root.transform.CompareTag("Player1"))
            {
                PlayerStatement.instance.p2AsBall = false;
                PlayerStatement.instance.p1AsBall = true;
            }
            else if (transform.root.transform.CompareTag("Player2"))
            {
                PlayerStatement.instance.p1AsBall = false;
                PlayerStatement.instance.p2AsBall = true;
            }

            BallMovements.instance.SetTarget(transform.root.gameObject);
            BallMovements.instance.isTaken = true;
        }
    }
}
