using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatement : MonoBehaviour
{
    public static PlayerStatement instance;
    
    public bool p1AsBall;
    public bool p1IsShooting;
    public int p1Stamina;
    
    public bool p2AsBall;
    public bool p2IsShooting;
    public int p2Stamina;

    private void Awake()
    {
        instance = this;
    }

    public bool PlayerHasTheBall(int i)
    {
        if (i == 1)
        {
            if (p1AsBall)
            {
                return true;
            }
            else
            {
                return false;
            }
        }else if (i == 2)
        {
            if (p2AsBall)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        return false;
    }
}
