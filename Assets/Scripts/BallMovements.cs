using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMovements : MonoBehaviour
{
    public static BallMovements instance;
    public Rigidbody rb;
    public GameObject player;
    public bool isTaken;

    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        if (isTaken)
        {
            transform.position = player.transform.position+(1.25f*player.transform.forward);
        }
    }

    public void SetTarget(GameObject go)
    {
        player = go;
    }

    public void Reset()
    {
        isTaken = false;
        transform.position = new Vector3(0, 1, 0);
        rb.velocity = Vector3.zero;
    }
    
}
