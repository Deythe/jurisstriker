using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class MakePoint : MonoBehaviour
{
    [SerializeField] private int team;

    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.CompareTag("Ball"))
        {
            ScoreManager.instance.MakePoint(team);
        }
    }
}
