using System;
using TMPro;
using UnityEngine;


public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;
    
    public int scoreP1=0;
    public int scoreP2=0;
    //[SerializeField] private TMP_Text scoreP1Text;
    //[SerializeField] private TMP_Text scoreP2Text;

    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        //scoreP1Text.text = "Score Player 1 : " + scoreP1;
        //scoreP2Text.text = "Score Player 2 : " + scoreP2;
    }

    public void MakePoint(int i)
    {
        if (i==1)
        {
            scoreP2++;
        }else if (i == 2)
        {
            scoreP1++;
        }
        
        BallMovements.instance.Reset();
        
        Debug.Log(scoreP1);
        Debug.Log(scoreP2);
    }
}
