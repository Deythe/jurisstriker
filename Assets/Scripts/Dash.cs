using System.Collections;
using UnityEngine;

public class Dash : MonoBehaviour
{
   private bool isCanDash = true;
   [SerializeField] private Rigidbody rb;
   [SerializeField] private float dashForce = 50;
   
   public void PlayerDash()
   {
      if (isCanDash)
      {
         StartCoroutine(CooldownDash());
         rb.AddForce(transform.forward*dashForce, ForceMode.Impulse);
      }
   }

   IEnumerator CooldownDash()
   {
      isCanDash = false;
      yield return new WaitForSeconds(2);
      isCanDash = true;
   }
}
