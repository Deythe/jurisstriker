using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private Rigidbody rb;
    [SerializeField] private float speed;
    
    public void Movement(float horizontal, float vertical)
    {
        rb.velocity = Vector3.right*horizontal*speed + Vector3.forward*vertical*speed + new Vector3(0,rb.velocity.y,0);
        transform.rotation = Quaternion.LookRotation(Vector3.right*horizontal+ Vector3.forward*vertical);
        
    }
}